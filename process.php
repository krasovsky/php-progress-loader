<?php

function ob_ignore($data, $flush = false)
    {
    $ob = array();
    while (ob_get_level())
        {
        array_unshift($ob, ob_get_contents());
        ob_end_clean();
        }
    
    echo $data;
    if ($flush)
        flush();
    
    foreach ($ob as $ob_data)
        {
        ob_start();
        echo $ob_data;
        }
    return count($ob);
    }

if (($work = @$_GET['work']) > 0)
    {
    header("X-Progress-Max: $work", true, 200);
    header("X-Progress-Padding: 20");
    ob_ignore(str_repeat(' ', 20), true);
    
    for ($i = 0; $i < $work; $i++)
        {
        usleep(rand(100000, 500000));
        ob_ignore(' ', true);
        }
    
    echo $work.' done!';
    die();
    }