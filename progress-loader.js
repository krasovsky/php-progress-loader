function ProgressLoader(url, callbacks)
    {
    var _this = this;
    for (var k in callbacks)
        if (typeof callbacks[k] != 'function')
            callbacks[k] = false;
    delete k;
    
    function getXHR()
        {
        var xhr;
        try
            {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
            }
        catch (e)
            {
            try
                {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
            catch (E)
                {
                xhr = false;
                }
            }
        if (!xhr && typeof XMLHttpRequest != 'undefined')
            xhr = new XMLHttpRequest();
        return xhr;
        }
    
    this.xhr = getXHR();
    this.xhr.open('GET', url, true);
    
    var contentLoading = false,
        progressPadding = 0,
        progressMax = -1,
        progress = 0,
        progressPerc = 0;
    
    this.xhr.onreadystatechange = function()
        {
        if (this.readyState == 2)
            {
            contentLoading = false;
            progressPadding = +this.getResponseHeader('X-Progress-Padding') || progressPadding;
            progressMax = +this.getResponseHeader('X-Progress-Max') || progressMax;
            if (callbacks.start)
                callbacks.start.call(_this, this.status);
            }
        else if (this.readyState == 3)
            {
            if (!contentLoading)
                contentLoading = !!this.responseText
                    .replace(/^\s+/, '');	// .trimLeft() — медленнее О_о
            
            if (!contentLoading)
                {
                progress = this.responseText.length - progressPadding;
                progressPerc = progressMax > 0 ? progress / progressMax : -1;
                if (callbacks.progress)
                    {
                    callbacks.progress.call(_this,
                        this.status,
                        progress,
                        progressPerc,
                        progressMax
                        );
                    }
                }
            else if (callbacks.loading)
                callbacks.loading.call(_this, this.status, this.responseText);
            }
        else if (this.readyState == 4)
            {
            if (callbacks.end)
                callbacks.end.call(_this, this.status, this.responseText);
            }
        };
    if (callbacks.abort)
        this.xhr.onabort = callbacks.abort;
    
    this.xhr.send(null);
    
    this.abort = function()
        {
        return this.xhr.abort();
        };
    
    this.getProgress = function()
        {
        return progress;
        };
    
    this.getProgressMax = function()
        {
        return progressMax;
        };
    
    this.getProgressPerc = function()
        {
        return progressPerc;
        };
    
    return this;
    }